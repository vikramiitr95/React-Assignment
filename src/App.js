import React from 'react';
import {HashRouter , Route} from 'react-router-dom';
import Header from './header';
//import Home from './home';
import Planet from './planet';

function MyApp() {
  
  return <HashRouter>
    <Header />
    <Route exact path="/planet" component={Planet} />

    </HashRouter>
}

export default MyApp;
