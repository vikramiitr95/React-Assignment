import React, { useEffect, useState } from 'react';
import axios from 'axios';

const Planet = () => {
    const [planetList, updateList] = useState([]);
    const [favplanet, updatePlanet] = useState([]);

    useEffect(() => {
        axios.get("https://assignment-machstatz.herokuapp.com/planet").then(response => {
            updateList(response.data);
        })
    }, []);

    const getMyplanet = (planetInfo) => {
        updatePlanet(favplanet => [...favplanet, planetInfo]);
    }

    return <div className="container">
        <div className="row">
            <div className="col-md-12">
                <h1 className="text-center">All Planets - {planetList.length}</h1>
                <table className="table table-bordered border-dark">
                    <thead className="text-center bg-primary">
                        <tr>
                            <th>id</th>
                            <th>isFavourite</th>
                            <th>name</th>
                            <th>is Favourite</th>
                        </tr>
                    </thead>
                    <tbody className="text-center">
                        {
                            planetList.map((xplanet, index) => {
                                return <tr key={index}>
                                    <td>{xplanet.id}</td>
                                    <td>{JSON.stringify(xplanet.isFavourite)}</td>
                                    <td>{xplanet.name}</td>
                                    <td><button className="btn btn-success" onClick={getMyplanet.bind(this, xplanet)}>Add to Favourite</button></td>
                                </tr>
                            })
                        }
                    </tbody>
                </table>
            </div>
        </div>

        <div className="row mt-5">
            <div className="col-md-12">
                <h1 className="text-center text-info">Favourite Planets - {favplanet.length}</h1>
                <div className="row text-center">
                    {
                        favplanet.map((xfavplanet, index) => {
                            return <div key={index} className="col-md-3">
                                        <p className="border p-2">{xfavplanet.name}</p>
                                    </div>

                        })
                    }
                </div>
            </div>
            </div>
        </div>

}

export default Planet;